﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Введіть ціле число N (> 0):");
            int N;
            while (!int.TryParse(Console.ReadLine(), out N))
            {
                Console.WriteLine("Некоректне значення. Введіть число для N:");
            }

            Console.WriteLine("Введіть ціле число N (> 0):");
            int K;
            while (!int.TryParse(Console.ReadLine(), out K))
            {
                Console.WriteLine("Некоректне значення. Введіть число для K:");
            }

            int sum = 0;

            if (N > 0 && K > 0)
            {
                for (int i = 1; i <= N; i++)
                {
                    sum += (int)Math.Pow(i, K);
                }
            }
            else
            {
                Console.WriteLine("Ти напевно ввів мінусове значення ");
                return;
            }
            Console.WriteLine($"Сума = {sum}");
        }
    }
}

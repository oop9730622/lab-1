﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace WpfApp2
{
  
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            double a, b, c;
            bool ok;
            ok = double.TryParse(TextBox1.Text, out a);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення a!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ok = double.TryParse(TextBox2.Text, out b);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення b!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ok = double.TryParse(TextBox3.Text, out c);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення c!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double D = Math.Pow(b, 2) - 4 * a * c;
            if (D > 0)
            {
                double x1 = (-b + Math.Sqrt(D)) / (2 * a);
                double x2 = (-b - Math.Sqrt(D)) / (2 * a);
                label4.Visibility= Visibility.Visible;
                label5.Visibility = Visibility.Visible;
                label6.Visibility = Visibility.Visible;
                TextBox4.Visibility = Visibility.Visible;
                TextBox5.Visibility = Visibility.Visible;
                TextBox6.Visibility = Visibility.Visible;
                TextBox4.Text = x1.ToString("F2");
                TextBox5.Text = x2.ToString("F2");
                TextBox6.Text = D.ToString("F2");
            }
            else if (D == 0)
            {
                double x0 = (-b / (2 * a));
                label4.Visibility = Visibility.Visible;
                TextBox4.Visibility = Visibility.Visible;

            }
            else
            {
                MessageBox.Show("Коренів немає", "Відповідь", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }
   
    }
}
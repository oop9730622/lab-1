﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LAB1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            double x, y, z;
            bool ok;
            do
            {
                Console.Write("Введіть дробове значення x = ");
                ok = double.TryParse(Console.ReadLine(), out x);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення x. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            do
            {
                Console.Write("Введіть дробове значення y = ");
                ok = double.TryParse(Console.ReadLine(), out y);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення y. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            do
            {
                Console.Write("Введіть дробове значення z = ");
                ok = double.TryParse(Console.ReadLine(), out z);
                if (!ok)
                    Console.WriteLine("  Помилка введення значення z. Будь-ласка повторіть введення значення ще раз!");
            } while (!ok);
            double part1 = Math.Pow(y, 1.0 / 4) + Math.Pow(x - 1, 1.0 / 3);
            double part2 = Math.Abs(x - y) * (Math.Pow(Math.Sin(z), 2) + Math.Tan(z));
            double S = part1 / part2;
            Console.WriteLine("Результат обчислення: s = {0:F3}", S);
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            int parne = 0;
            int neparne = 0;
            int positive = 0;
            int negative = 0;

            Console.WriteLine("Введіть цілі числа (введіть 0 для завершення введення):");

            while (true)
            {
                int n;
                while (!int.TryParse(Console.ReadLine(), out n))
                {
                    Console.WriteLine("Некоректне значення. Введіть число для N:");
                }
                if (n == 0)
                    break;

                if (n % 2 == 0)
                    parne++;
                else
                    neparne++;

                if (n > 0)
                    positive++;
                else if (n < 0)
                    negative++;
            }

            Console.WriteLine($"Кількість парних чисел: {parne}");
            Console.WriteLine($"Кількість непарних чисел: {neparne}");
            Console.WriteLine($"Кількість додатних чисел: {positive}");
            Console.WriteLine($"Кількість від'ємних чисел: {negative}");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.WriteLine("Введіть коефіцієнти a, b і c для квадратного рівняння ax^2 + bx + c = 0:");

            double a, b, c;
            Console.Write("a = ");
            while (!double.TryParse(Console.ReadLine(), out a))
            {
                Console.WriteLine("Некоректне значення. Введіть число для a:");
            }

            Console.Write("b = ");
            while (!double.TryParse(Console.ReadLine(), out b))
            {
                Console.WriteLine("Некоректне значення. Введіть число для b:");
            }

            Console.Write("c = ");
            while (!double.TryParse(Console.ReadLine(), out c))
            {
                Console.WriteLine("Некоректне значення. Введіть число для c:");
            }

            double d = b * b - 4 * a * c;

            if (d > 0)
            {
                double x1 = (-b + Math.Sqrt(d)) / (2 * a);
                double x2 = (-b - Math.Sqrt(d)) / (2 * a);
                Console.WriteLine($"Дискримінант = {d}, Розв'язки: x1 = {x1}, x2 = {x2}");
            }
            else if (d == 0)
            {
                double x = -b / (2 * a);
                Console.WriteLine($"Дискримінант = {d}, Розв'язок: x = {x}");
            }
            else
            {
                Console.WriteLine("Дискримінант від'ємний, розв'язків немає.");
            }
            Console.ReadKey();
        }
    }
}

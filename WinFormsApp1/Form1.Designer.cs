﻿namespace WinFormsApp1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            button1 = new Button();
            labelX = new Label();
            textBoxX = new TextBox();
            textBoxY = new TextBox();
            textBoxZ = new TextBox();
            textBoxS = new TextBox();
            labelY = new Label();
            labelZ = new Label();
            labelS = new Label();
            label1 = new Label();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            button1.Location = new Point(109, 147);
            button1.Name = "button1";
            button1.Size = new Size(126, 36);
            button1.TabIndex = 0;
            button1.Text = "Count";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // labelX
            // 
            labelX.AutoSize = true;
            labelX.Location = new Point(21, 17);
            labelX.Name = "labelX";
            labelX.Size = new Size(14, 15);
            labelX.TabIndex = 1;
            labelX.Text = "X";
            // 
            // textBoxX
            // 
            textBoxX.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            textBoxX.Location = new Point(77, 14);
            textBoxX.Name = "textBoxX";
            textBoxX.Size = new Size(158, 23);
            textBoxX.TabIndex = 2;
            // 
            // textBoxY
            // 
            textBoxY.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            textBoxY.Location = new Point(77, 55);
            textBoxY.Name = "textBoxY";
            textBoxY.Size = new Size(158, 23);
            textBoxY.TabIndex = 3;
            // 
            // textBoxZ
            // 
            textBoxZ.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            textBoxZ.Location = new Point(77, 95);
            textBoxZ.Name = "textBoxZ";
            textBoxZ.Size = new Size(158, 23);
            textBoxZ.TabIndex = 4;
            // 
            // textBoxS
            // 
            textBoxS.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            textBoxS.Location = new Point(77, 210);
            textBoxS.Name = "textBoxS";
            textBoxS.Size = new Size(158, 23);
            textBoxS.TabIndex = 5;
            // 
            // labelY
            // 
            labelY.AutoSize = true;
            labelY.Location = new Point(21, 58);
            labelY.Name = "labelY";
            labelY.Size = new Size(14, 15);
            labelY.TabIndex = 6;
            labelY.Text = "Y";
            // 
            // labelZ
            // 
            labelZ.AutoSize = true;
            labelZ.Location = new Point(21, 98);
            labelZ.Name = "labelZ";
            labelZ.Size = new Size(14, 15);
            labelZ.TabIndex = 7;
            labelZ.Text = "Z";
            // 
            // labelS
            // 
            labelS.AutoSize = true;
            labelS.Location = new Point(21, 213);
            labelS.Name = "labelS";
            labelS.Size = new Size(13, 15);
            labelS.TabIndex = 8;
            labelS.Text = "S";
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            label1.AutoSize = true;
            label1.Location = new Point(468, 426);
            label1.Name = "label1";
            label1.Size = new Size(114, 15);
            label1.TabIndex = 9;
            label1.Text = "Khmil D. U. IPZ-23-1";
            label1.Click += label1_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(584, 461);
            Controls.Add(label1);
            Controls.Add(labelS);
            Controls.Add(labelZ);
            Controls.Add(labelY);
            Controls.Add(textBoxS);
            Controls.Add(textBoxZ);
            Controls.Add(textBoxY);
            Controls.Add(textBoxX);
            Controls.Add(labelX);
            Controls.Add(button1);
            MinimumSize = new Size(600, 500);
            Name = "Form1";
            Text = "Лабораторна №1.Завдання 1";
            Load += Form1_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button button1;
        private Label labelX;
        private TextBox textBoxX;
        private TextBox textBoxY;
        private TextBox textBoxZ;
        private TextBox textBoxS;
        private Label labelY;
        private Label labelZ;
        private Label labelS;
        private Label label1;
    }
}
